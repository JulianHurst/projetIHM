package agenda.ihm.com.agenda;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juju on 02/01/17.
 */

public class RecetteFragment extends Fragment {

    private List<AgendaRecette> list = new ArrayList<>();

    public RecetteFragment(){}

    public static RecetteFragment newInstance(int page, ArrayList<AgendaRecette> list) {
        RecetteFragment fragmentFirst = new RecetteFragment();
        Bundle args = new Bundle();
        args.putInt("", page);
        args.putParcelableArrayList("list",list);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        list=getArguments().getParcelableArrayList("list");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_agenda, container, false);
        final ListView lv = (ListView) rootView.findViewById(R.id.tasklist);
        ArrayAdapter<AgendaRecette> adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,android.R.id.text1,list);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), ModifRecette.class);
                AgendaRecette a = (AgendaRecette) lv.getItemAtPosition(position);
                intent.putExtra("name",a.getName());
                intent.putExtra("pos",position);
                intent.putExtra("tabpos",2);
                getActivity().startActivityForResult(intent,Agenda.MODIF_RECETTE);
            }
        });
        return rootView;
    }
}
