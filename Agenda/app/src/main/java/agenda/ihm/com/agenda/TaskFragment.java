package agenda.ihm.com.agenda;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by juju on 30/12/16.
 */

public class TaskFragment extends Fragment {

    //private static int MODIF_TASK=0;

    private List<AgendaTask> list = new ArrayList<>();

    private ListView lv;

    private ArrayAdapter<AgendaTask> adapter;

    public TaskFragment(){}

    public static TaskFragment newInstance(int page, ArrayList<AgendaTask> list) {
        TaskFragment fragmentFirst = new TaskFragment();
        Bundle args = new Bundle();
        args.putInt("", page);
        args.putParcelableArrayList("list",list);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        list=getArguments().getParcelableArrayList("list");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_agenda, container, false);
        lv = (ListView) rootView.findViewById(R.id.tasklist);
        adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,android.R.id.text1,list);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), ModifTask.class);
                AgendaTask a = (AgendaTask) lv.getItemAtPosition(position);
                intent.putExtra("name",a.getName());
                intent.putExtra("date",a.getDate());
                intent.putExtra("time",a.getTime());
                intent.putExtra("pos",position);
                getActivity().startActivityForResult(intent,Agenda.MODIF_TASK);
            }
        });
        return rootView;
    }
}
