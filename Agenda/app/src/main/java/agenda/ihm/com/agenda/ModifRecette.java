package agenda.ihm.com.agenda;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class ModifRecette extends AppCompatActivity {

    private EditText E;
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recette);
        E = (EditText) findViewById(R.id.name);

        Bundle fields=getIntent().getExtras();
        if(fields!=null)
            setFields(fields);

        E.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        Button B = (Button) findViewById(R.id.create);
        B.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(getApplicationContext(), Agenda.class);
                AgendaRecette A = new AgendaRecette(E.getText().toString());
                I.putExtra("recette",A);
                I.putExtra("pos",pos);
                setResult(RESULT_OK,I);
                finish();
            }
        });
    }

    public void setFields(Bundle fields){
        String name;
        name=fields.getString("name");
        pos=fields.getInt("pos");
        if(name!=null)
            E.setText(name);
    }
}
