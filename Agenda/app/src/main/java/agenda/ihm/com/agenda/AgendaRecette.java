package agenda.ihm.com.agenda;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by juju on 02/01/17.
 */

public class AgendaRecette implements Parcelable {
    String name;

    public AgendaRecette(String name){
        this.name=name;
    }


    protected AgendaRecette(Parcel in) {
        name = in.readString();
    }

    public static final Creator<AgendaRecette> CREATOR = new Creator<AgendaRecette>() {
        @Override
        public AgendaRecette createFromParcel(Parcel in) {
            return new AgendaRecette(in);
        }

        @Override
        public AgendaRecette[] newArray(int size) {
            return new AgendaRecette[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }

    public String getName(){
        return name;
    }

    public String toString(){
        return name;
    }
}
