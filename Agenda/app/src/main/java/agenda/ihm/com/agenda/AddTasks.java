package agenda.ihm.com.agenda;

/**
 * Created by Patty on 30/12/2016.
 */
import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class AddTasks extends ListActivity {

    /** Called when the activity is first created. */
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        // create an array of Strings, that will be put to our ListActivity
        ArrayAdapter<ModelList> adapter = new InteractiveArrayAdapter(this,
                getModel());
        setListAdapter(adapter);
    }

    public List<ModelList> getModel() {
        List<ModelList> list = new ArrayList<ModelList>();
        list.add(get("Faire sport"));
        list.add(get("Manger à 19h"));
        list.add(get("Faire le linge"));
        list.add(get("Diner à 20h"));
        list.add(get("Appler Linda"));
        // Initially select one of the items
        list.get(1).setSelected(true);
        return list;
    }

    public ModelList get(String s) {
        return new ModelList(s);
    }

}
