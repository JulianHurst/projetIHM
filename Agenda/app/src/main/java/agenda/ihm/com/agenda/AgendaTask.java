package agenda.ihm.com.agenda;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by juju on 02/01/17.
 */

public class AgendaTask implements Parcelable {
    private String name;
    private String date;
    private String time;

    public AgendaTask(String name,String date,String time){
        this.name=name;
        this.date=date;
        this.time=time;
    }


    protected AgendaTask(Parcel in) {
        name = in.readString();
        date = in.readString();
        time = in.readString();
    }

    public static final Creator<AgendaTask> CREATOR = new Creator<AgendaTask>() {
        @Override
        public AgendaTask createFromParcel(Parcel in) {
            return new AgendaTask(in);
        }

        @Override
        public AgendaTask[] newArray(int size) {
            return new AgendaTask[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(date);
        dest.writeString(time);
    }

    public String getName(){
        return name;
    }

    public String getDate(){
        return date;
    }

    public String getTime(){
        return time;
    }

    public void setName(String name) { this.name=name; }

    public void setDate(String date) { this.date=date; }

    public void setTime(String time) { this.time=time; }

    public String toString(){
        return name+"\n"+date+"\n"+time;
    }
}
