package agenda.ihm.com.agenda;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class AddTask extends AppCompatActivity {
    EditText Ename;
    EditText Edate;
    EditText Etime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        Ename = (EditText) findViewById(R.id.name);
        Edate = (EditText) findViewById(R.id.date);
        Etime = (EditText) findViewById(R.id.time);


        Bundle fields = getIntent().getExtras();

        Ename.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        Button B = (Button) findViewById(R.id.create);
        B.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = Ename.getText().toString();
                String date = Edate.getText().toString();
                String time = Etime.getText().toString();
                if (name.isEmpty()) {
                    System.out.println("Name inco");
                    return;
                }
                if (!date.matches("\\d{2}(/|\\.|\\-)\\d{2}(/|\\.|\\-)\\d{4}")) {
                    System.out.println("date inco");
                    return;
                }
                if (!time.matches("\\d{2}:\\d{2}")) {
                    System.out.println("time inco");
                    return;
                }
                Intent I = new Intent(getApplicationContext(), Agenda.class);
                AgendaTask T = new AgendaTask(name, date, time);
                setResult(RESULT_OK, I.putExtra("task", T));
                finish();
            }
        });
    }
}
