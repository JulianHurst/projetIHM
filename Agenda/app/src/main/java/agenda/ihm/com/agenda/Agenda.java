package agenda.ihm.com.agenda;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;


import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

public class Agenda extends AppCompatActivity {

    protected static int ADD_TASK = 0;
    protected static int ADD_RECETTE = 1;
    protected static int MODIF_TASK = 2;
    protected static int MODIF_RECETTE = 3;
    protected static int ERROR = 99;

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private static ArrayList<AgendaTask> taskList=new ArrayList<>();
    private static ArrayList<AgendaRecette> recList=new ArrayList<>();

    private static int currentPosition;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    public Agenda() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),getApplicationContext());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                int position=tabLayout.getSelectedTabPosition();
                int requestCode;
                switch (position){
                    case 0:
                        intent = new Intent(getApplicationContext(), AddTask.class);
                        requestCode=ADD_TASK;
                        break;
                    case 1:
                        intent = new Intent(getApplicationContext(), AddTask.class);
                        requestCode=ADD_TASK;
                        break;
                    case 2:
                        intent = new Intent(getApplicationContext(), AddRecette.class);
                        requestCode=ADD_RECETTE;
                        break;
                    default:
                        intent = null;
                        requestCode=ERROR;
                }
                startActivityForResult(intent,requestCode);
            }
        });



        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_agenda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            //setContentView(R.layout.activity_login);
            Intent intent=new Intent(getApplicationContext(), Login.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Agenda Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public void onPause(){
        super.onPause();
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        currentPosition=tabLayout.getSelectedTabPosition();
    }

    @Override
    public void onResume(){
        super.onResume();
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),getApplicationContext());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.getTabAt(currentPosition).select();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode==ADD_TASK){
            if(resultCode==RESULT_OK){
                AgendaTask T = data.getParcelableExtra("task");
                taskList.add(T);
            }
        }
        else if(requestCode==ADD_RECETTE){
            if(resultCode==RESULT_OK){
                AgendaRecette R = data.getParcelableExtra("recette");
                recList.add(R);
            }
        }
        else if(requestCode==MODIF_TASK){
            if(resultCode==RESULT_OK){
                AgendaTask T = data.getParcelableExtra("task");
                int position = data.getIntExtra("pos",0);
                taskList.set(position,T);
            }
        }
        else if(requestCode==MODIF_RECETTE){
            if(resultCode==RESULT_OK){
                AgendaRecette T = data.getParcelableExtra("recette");
                int position = data.getIntExtra("pos",0);
                recList.set(position,T);
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public static class SectionsPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 3;
        private final Context ctx;

        public SectionsPagerAdapter(FragmentManager fragmentManager,Context ctx) {
            super(fragmentManager);
            this.ctx=ctx;
        }

        @Override
        public Fragment getItem(int position) {
            ArrayList<? extends Parcelable> l = new ArrayList<>();
            switch (position) {
                case 0:
                    l=Agenda.taskList;
                    return TaskFragment.newInstance(0, (ArrayList<AgendaTask>) l);
                case 1:
                    //l.add("Calendrier");
                    return CalFragment.newInstance();
                case 2:
                    l=Agenda.recList;
                    return RecetteFragment.newInstance(2, (ArrayList<AgendaRecette>) l);
                default:
                    ArrayList<AgendaRecette> j=new ArrayList<>();
                    AgendaRecette S = new AgendaRecette("Erreur");
                    j.add(S);
                    l=j;
                    return RecetteFragment.newInstance(3, (ArrayList<AgendaRecette>) l);
            }
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return ctx.getString(R.string.task_col);
                case 1:
                    return ctx.getString(R.string.cal_col);
                case 2:
                    return ctx.getString(R.string.rec_col);
                default:
                    return "Erreur";
            }
        }
    }
}