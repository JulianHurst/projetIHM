package agenda.ihm.com.agenda;

/**
 * Created by Patty on 30/12/2016.
 */

public class ModelList {

        private String name;
        private boolean selected;

        public ModelList(String name) {
            this.name = name;
            selected = false;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

}
