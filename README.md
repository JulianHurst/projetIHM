# Projet de IHM du Master II GL de St Jérôme.
## Membres du projet : Kevin GARABEDIAN, Julian HURST , Patty RANDRIAMBOLOLONA et Hamza SERHROUCHNI

#### Dossiers :
- [Documents (Rapport de projet)](https://gitlab.com/JulianHurst/projetIHM/tree/master/Doc)
- [Persona](https://gitlab.com/JulianHurst/projetIHM/tree/master/persona)
- [Diagramme use-case](https://gitlab.com/JulianHurst/projetIHM/tree/master/Diagramme)
- [Mockups](https://gitlab.com/JulianHurst/projetIHM/tree/master/mockup)
- [Projet Android](https://gitlab.com/JulianHurst/projetIHM/tree/master/Agenda)
